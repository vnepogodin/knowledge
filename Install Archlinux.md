## Instruction

```
fdisk -l
```

### Choose your disk

```
cfdisk /dev/*
```

### Edit partition (only bios)
___
1. Boot partition 216-512mb (select bootable, primary)
2. Swap more than 512mb (choose linux swap, primary)
3. Simple partition (choose linux, executable)
___

### Format the partitions

###### Once the partitions have been created, each must be formatted with an appropriate file system. For example, if the root partition is on `/dev/sdX1` and will contain the ext4 file system, run:

```
mkfs.ext4 /dev/sdX1
```

###### For swap, initialize it with mkswap:


```
mkswap /dev/sdX2
___
swapon /dev/sdX2
```
### Mount the file systems

###### Mount the file system on the root partition to `/mnt`, for example:

```
mount /dev/sdX1 /mnt
```

###### Create folders

```
mkdir /mnt/boot /mnt/var /mnt/etc
```


## Installation

###### Use the pacstrap script to install the base package, Linux kernel and firmware for common hardware:

```
pacstrap /mnt base linux linux-firmware
```


## Configure the system
### Fstab

###### Generate an fstab file:

```
genfstab -U /mnt >> /mnt/etc/fstab
```


### Chroot

###### Change root into the new system:

```
arch-chroot /mnt
```

### Time zone

###### Set the time zone:

```
ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
```

###### Run hwclock to generate `/etc/adjtime`:

```
hwclock --systohc
```

## Set the hostname

###### The file can contain the system's domain name, if any. To set the hostname, edit `/etc/hostname to` include a single line with myhostname: 

```
hostnamectl set-hostname myhostname
```

###### or

```
hostname myhostname
```
