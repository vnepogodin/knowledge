# iMovie
Преврати свои видео в киноволшебство.

![Preview](https://www.apple.com/v/imovie/c/images/overview/imovie__fpwdwkggrdme_large.png)

## Пример
* [Трейлер](https://support.apple.com/guide/imovie/create-a-new-trailer-project-mov6e14403f2/10.1.15/mac/10.15.5)

## Обучение
- Видео
    * [Основы редактора](https://support.apple.com/guide/imovie/welcome/mac)
    * [Звук](https://support.apple.com/guide/imovie/add-music-and-sound-clips-mov91a895a64/10.1.15/mac/10.15.5)
    * [Другое](https://support.apple.com/guide/imovie/toc)

## Установка
#### iOS
- [Скачать](https://apps.apple.com/us/app/imovie/id377298193?amp%3Bv0=www-us-ios-imovie-app-imovie)
#### macOS
- [Скачать](https://apps.apple.com/us/app/imovie/id408981434?amp%3Bls=1&amp%3Bv0=www-us-mac-imovie-app-imovie&mt=12)
