# FlowBlade
Flowblade - это **многодорожный не линейный видео редактор** для Linux

![Preview](https://raw.githubusercontent.com/jliljebl/flowblade/master/flowblade-trunk/docs/Screenshot-2-0.png)

## Обучение
- Текст
    * [Основы редактора](https://www.ubuntubuzz.com/2016/12/beginners-guide-for-flowblade-video-editor.html)
- Видео
    * [Основы редактора](https://youtu.be/o1LXACbL9Sg)
    * [Замедленние, ускоренние](https://youtu.be/R5QW8sF9oOw)
    * [Перевернутое видео](https://youtu.be/pE_jExnUwec)
    * [Цвето-коррекция](https://youtu.be/offO5uKAWEU)
    * [Соотношение сторон](https://youtu.be/OCefnPnGOyc)

## Установка
#### Debian/Ubuntu
```bash
sudo apt install flowblade
```
#### Archlinux
```bash
sudo pacman -Sy flowblade
```
