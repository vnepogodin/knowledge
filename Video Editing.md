# Бук Трейлер
Бук Трейлер - короткий рекламный видеоролик по мотивам книги.

## Примеры
- [Clementine](https://www.youtube.com/watch?v=m6sgd2n0eFI)
- [Mecanoscrit del segon orígen](https://youtu.be/AIkSmbZxpI0)
- [13 причин почему](https://youtu.be/oecG3Wgp4y0)

## Редакторы
#### macOS
![Preview](https://www.apple.com/v/imovie/c/images/overview/imovie_everywhere__9yx2sqjfj5u6_large.png)
- [Редактор](https://gitlab.com/vnepogodin/knowledge/-/blob/master/iMovie.md)
#### Windows
- [Редактор](https://gitlab.com/vnepogodin/knowledge/-/blob/master/olive.md)
#### Linux
![Preview](https://github.com/jliljebl/flowblade/raw/master/flowblade-trunk/docs/1-10-timeline_match_frame.jpg)
- [Редактор](https://gitlab.com/vnepogodin/knowledge/-/blob/master/flowblade.md)
