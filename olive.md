# Olive
Olive - это бесплатный не линейный видео редактор для Windows, macOS и Linux.

![Preview](https://camo.githubusercontent.com/6cf4322470c6f647a73f3484044d6e6b4013af9f/68747470733a2f2f7777772e6f6c697665766964656f656469746f722e6f72672f696d672f73637265656e73686f742e6a7067)

## Пример
* [Как сделать Slideshow за несколько секунд](https://youtu.be/ypldwJjxYoM)

## Обучение
- Видео
    * [Основы редактора](https://youtu.be/R196PezaG_E)
    * [Звук](https://youtu.be/IEmAnLzrrqY)
    * [Keyframe](https://youtu.be/2ztNV7dZJTc)

## Установка
#### Windows
- [Скачать](https://olivevideoeditor.org/go.php?d=w64p)
- Распаковать архив.
- Запустить `olive/olive-editor.exe`.
#### macOS
- [Скачать](https://olivevideoeditor.org/go.php?d=m64d)
- Распаковать архив.
- Запустить `Olive.app`.
#### Debian/Ubuntu
```bash
sudo apt install olive
```
#### Archlinux
```bash
sudo pacman -Sy olive
```
